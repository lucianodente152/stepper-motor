import { ComponentFixture, TestBed } from '@angular/core/testing';
import { AppComponent } from './app.component';
import { AppModule } from './app.module';
import { DynamicViewDirective } from './directive/dynamic-view.directive';

describe('AppComponent', () => {

  let component: AppComponent;
  let fixture: ComponentFixture<AppComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [AppModule],
      providers: [DynamicViewDirective],
      declarations: [AppComponent]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AppComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create the app', () => {
    expect(component).toBeTruthy();
  });

  it(`should have as title 'stepper-motor'`, () => {
    expect(component.title).toEqual('stepper-motor');
  });

  it('should render Name Component', () => {
    const compiled = fixture.nativeElement as HTMLElement;
    expect(compiled.querySelector('h3.col.col-10')?.textContent).toContain('Name Component');
  });
});
