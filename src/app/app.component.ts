import { AfterViewInit, ChangeDetectorRef, Component, Type, ViewChild } from '@angular/core';
import { GenericFormComponent } from './component/generic-form/generic-form.component';
import { DynamicViewDirective } from './directive/dynamic-view.directive';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements AfterViewInit {
  @ViewChild(DynamicViewDirective) dynamicDirectiveView!: DynamicViewDirective;
  title = 'stepper-motor';

  constructor(
    private changeDetectorRef: ChangeDetectorRef,
  ) { }

  ngAfterViewInit(): void {
    this.dynamicDirectiveView.view = this.currentStep as Type<Component>;
    this.changeDetectorRef.detectChanges();
  }

  private get currentStep(): Component {
    return GenericFormComponent as Component;
  }

  public goNext() { }

}
