import { Component, Directive, Type, ViewContainerRef } from '@angular/core';

@Directive({
  selector: '[appDynamicView]'
})
export class DynamicViewDirective {

  constructor(
    private viewContainerRef: ViewContainerRef,
  ) { }

  public set view(component: Type<Component>) {
    this.viewContainerRef.clear();
    this.viewContainerRef.createComponent<Component>(component);
  }

}
