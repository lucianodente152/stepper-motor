import { ViewContainerRef } from '@angular/core';
import { TestBed } from '@angular/core/testing';
import { AppModule } from '../app.module';
import { DynamicViewDirective } from './dynamic-view.directive';

describe('DynamicViewDirective', () => {

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [AppModule],
      providers: [ViewContainerRef],
      declarations: [DynamicViewDirective]
    })
  });

  it('should create an instance', () => {
    const absView = TestBed.inject(ViewContainerRef);
    const directive = new DynamicViewDirective(absView);
    expect(directive).toBeTruthy();
  });
});
