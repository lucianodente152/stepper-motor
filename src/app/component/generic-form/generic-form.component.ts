import { Component, OnInit } from '@angular/core';
import { Province } from 'src/app/service/get-data/get-province.interface';
import { GetProvinceService } from 'src/app/service/get-data/get-province.service';

@Component({
  selector: 'app-generic-form',
  templateUrl: './generic-form.component.html',
})
export class GenericFormComponent implements OnInit {

  province: Province = {} as Province;

  constructor(private getProvinceService: GetProvinceService) { }

  ngOnInit(): void {
    this.getProvince();
  }

  getProvince(): void{
    this.getProvinceService
      .getProvince()
      .subscribe((response) => {
        this.province = response.provincias[0] as Province
      });
  }

}
