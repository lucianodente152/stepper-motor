import { ComponentFixture, TestBed } from '@angular/core/testing';
import { Observable, of, throwError } from 'rxjs';
import { Province, Response } from 'src/app/service/get-data/get-province.interface';
import { GetProvinceService } from 'src/app/service/get-data/get-province.service';

import { GenericFormComponent } from './generic-form.component';

const mockResponseGetProvince: Response = {
  cantidad: 1,
  inicio: 0,
  parametros: {
    nombre: "Sgo. del Estero"
  },
  provincias: [{
    centroide: {
      lat: -27.7824116550944,
      lon: -63.2523866568588
    },
    id: "86",
    nombre: "Santiago del Estero"
  }],
  total: 1
}

const mockGetProvinceService: { getProvince(): Observable<Response>; } = {
  getProvince(): Observable<Response> { return of(mockResponseGetProvince); }
}

describe('GenericFormComponent', () => {
  let component: GenericFormComponent;
  let fixture: ComponentFixture<GenericFormComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [
      ],
      providers: [
        { provide: GetProvinceService, useValue: mockGetProvinceService },

      ],
      declarations: [GenericFormComponent]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(GenericFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should have th in table with text Nombre Province', () => {
    const compiled = fixture.nativeElement as HTMLElement;
    expect(compiled.querySelector('th.text-center')?.textContent).toEqual('Nombre Province');
  });

  it('should call getProvince of getProvinceService, getProvince(): Observable<Response>', () => {
    const getProvinceSpy = spyOn(mockGetProvinceService, 'getProvince');
    getProvinceSpy.and.returnValue(of(mockResponseGetProvince));
    component.getProvince();
    expect(mockGetProvinceService.getProvince).toHaveBeenCalled();
  });

  it('should have a {} for throw Exception in getProvince of getProvinceService, getProvince(): Observable<Response>', () => {
    const getProvinceSpy = spyOn(mockGetProvinceService, 'getProvince');
    getProvinceSpy.and.returnValue(throwError(() => new Error("404")));

    //Se vuelve a generar por que se llama la primera vez en el ngOnInit
    fixture = TestBed.createComponent(GenericFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();

    expect(mockGetProvinceService.getProvince).toHaveBeenCalled();
    expect(component.province).toEqual({} as Province);
  });

});
