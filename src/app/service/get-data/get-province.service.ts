import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Response } from './get-province.interface';
import { Observable } from 'rxjs';

@Injectable({ providedIn: 'root' })
export class GetProvinceService {
  constructor(private httpClient: HttpClient) { }

  getProvince(): Observable<Response> {
    return this.httpClient.get('https://apis.datos.gob.ar/georef/api/provincias?nombre=Sgo.%20del%20Estero') as Observable<Response>;
  }

}