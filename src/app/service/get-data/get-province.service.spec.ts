import { HttpClient, HttpClientModule } from '@angular/common/http';
import { TestBed } from '@angular/core/testing';

import { GetProvinceService } from './get-province.service';

describe('GetProvinceService', () => {
  let service: GetProvinceService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        HttpClientModule
      ],
      providers: [{
        provide: HttpClient
      }]
    });
    service = TestBed.inject(GetProvinceService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

});
