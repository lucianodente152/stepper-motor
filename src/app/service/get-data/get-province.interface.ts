export interface Response {
    cantidad: 1,
    inicio: 0,
    parametros: { "nombre": "Sgo. del Estero" },
    provincias: [
        {
            centroide: {
                lat: -27.7824116550944,
                lon: -63.2523866568588
            },
            id: "86",
            nombre: "Santiago del Estero"
        }
    ],
    total: 1
}

export interface Province {
    centroide: {
        lat: -27.7824116550944,
        lon: -63.2523866568588
    },
    id: "86",
    nombre: "Santiago del Estero"
}